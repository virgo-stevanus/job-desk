<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\employee;
use App\Models\companies;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class employeeTest extends TestCase
{
    use DatabaseTransactions;
    public $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }
    
    /**
     * @test
     */
    public function user_can_go_to_employee_list_page()
    {
        
        $response = $this->get(route('employee'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_can_go_to_create_page()
    {
        $response = $this->get(route('inputemplo'));

        $response->assertStatus(200);
    }


    /**
     * @test
     */
    public function user_can_submit_list_employee()
    {
        $user = user::factory()->create();
        $companie = companies::factory()->create();
        $employee = employee::factory()->create();
        $response = $this->post(route('submitemplo'));
        
        $this->assertDatabaseHas('employees',[
            'name' => $employee->name,
            'password' => $employee->password,
            'company_id' => $employee->company_id,
            'city' => $employee->city,
            'email' => $employee->email,
            'phone' => $employee->phone,
            'created_users_id' => $employee->created_users_id,
            'updated_users_id' => $employee->updated_users_id,
            
           
        ]);
    }


    /**
     * @test
     */
    public function user_can_go_to_edit_page()
    {
        $employee = employee::factory()->create();
        $response = $this->get(route('editemplo',$employee->id));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_can_update_list_employee()
    {
        $employee= employee::factory()->create();
        $response = $this->put(route('updateemplo',$employee->id));
        $this->assertDatabaseHas('employees',[
            'name' => $employee->name,
            'password' => $employee->password,
            'company_id' => $employee->company_id,
            'city' => $employee->city,
            'email' => $employee->email,
            'phone' => $employee->phone,
            'created_users_id' => $employee->created_users_id,
            'updated_users_id' => $employee->updated_users_id,
            
           
        ]);

    }

    /**
     * @test
     */
    public function user_can_delete_list_employee()
    {
        $employee = employee::factory()->create();
        $response = $this->get(route('deleteemplo',$employee->id));

        $this->assertDatabaseMissing('employees', [
            'id' => $employee->id
        ]);
    }
}
