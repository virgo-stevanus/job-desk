<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\companies;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use function PHPSTORM_META\registerArgumentsSet;

class companieTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }
    /**
     * @test
     */
    public function view_companie_page()
    {
        $response = $this->get('/companies');
        $response->assertStatus(200);
    }

    /**
     * @test
     */

    public function user_can_go_to_create_companie_page()
    {
        
        $response = $this->get('/inputcompanies');
        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_can_create_companie()
    {
        $companies = companies::factory()->create();
        $response = $this->post(route('submitcomp'));  
        
        $this->assertDatabaseHas('companies', [
            'company_name' => $companies->company_name,
            'address' => $companies->address,
            'city' => $companies->city,
            'country' => $companies->country,
            'image' => null,
            'created_users_id' => $companies->created_users_id,
            'updated_users_id' => $companies->updated_users_id,
        ]);

        $response->assertStatus(302);
    
       
        
    }

    /**
     * @test
     */

    public function user_can_go_edit_page()
    {
        $companie = companies::factory()->create();
        
        $response = $this->get(route('editcomp', $companie->id));
        $response->assertStatus(200);

    }


    /**
     * @test
     */
    public function user_can_update_page()
    {
        $companie = companies::factory()->create();
        $response = $this->put(route('updatecomp', $companie->id));
        
        $this->assertDatabaseHas('companies',[
            'company_name' => $companie->company_name,
            'address' => $companie->address,
            'city' => $companie->city,
            'country' => $companie->country,
            'image' => null,
            'created_users_id' => $companie->created_users_id,
            'updated_users_id' => $companie->updated_users_id,
        ]);
        
    }



    /**
     * @test
     */
    public function user_can_delete_companie()
    {
        $companies = companies::factory()->create();
        $response = $this->get(route('deletecomp', $companies->id));

        $this->assertDatabaseMissing('companies', [
            'id' => $companies->id,
        ]);
        
    }
}