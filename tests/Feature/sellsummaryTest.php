<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class sellsummaryTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public $user;
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }

    /**
     * @test
     */
    public function user_can_view_sell_summaries_page()
    {
        $response = $this->get(route('summary'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_can_view_detail_sell_summaries_page()
    {
        $response = $this->get(route('detailsummary', 1));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_can_search_sell_summary()
    {
        $response = $this->get(route('searchsummary'));

        $response->assertStatus(200);
    }
}
