<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;


use App\Models\employee;
use App\Models\companies;
use Faker\Factory;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use function PHPUnit\Framework\assertJson;

class APITest extends TestCase
{   
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */


    /**
     * @test
     */
    public function register_user_api()
    {

        $response = $this->post(route('registerAPI', [
            'name' => 'testapi',
            'email' => 'initestapi@gmail.com',
            'password' => 'password'
        ]));

        $response->assertStatus(201);
    }

     /**
     * @test
     */
    public function validation_register_fails_name()
    {
        
        $user = User::create([
            'name' => ' ',
            'email' => 'initestapi@gmail.com',
            'password' => 'password'
        ]);

        $response = $this->post(route('registerAPI', $user));
        
        $response->assertStatus(400);
        
    }

     /**
     * @test
     */
    public function validation_register_fails_emails()
    {
        
        $user = User::create([
            'name' => 'test',
            'email' => 'initestapi',
            'password' => 'pass'
        ]);

        $response = $this->post(route('registerAPI', $user));
        
        $response->assertStatus(400);
        
    }

    /**
     * @test
     */
    public function validation_register_fails_password()
    {
        
        $user = User::create([
            'name' => 'test',
            'email' => 'initestapi@gmail.com',
            'password' => 'pass'
        ]);

        $response = $this->post(route('registerAPI', $user));
        
        $response->assertStatus(400);
        
    }

    /**
     * @test
     */
    public function login_api()
    {   
        // make new user first
        $this->post(route('registerAPI', [
            'name' => 'testapi2',
            'email' => 'initestapi2@gmail.com',
            'password' => 'password'
        ]));
        

        // login with new user
        $response = $this->post(route('loginAPI', [
            'email' => 'initestapi2@gmail.com',
            'password' => 'password'
        ]));


        $response->assertStatus(200)
        ->assertJsonStructure([
            'status',
            'token',
            'token_type'
        ]);
    }

    /**
     * @test
     */
    public function validation_login_api_fails_missing_password()
    {
        $response = $this->post(route('loginAPI', [
            'email' => 'admin2@gmail.com',

        ]));

        $response->assertInvalid();
    }

    /**
     * @test
     */
    public function validation_login_api_fails_missing_email()
    {
        $response = $this->post(route('loginAPI', [
            'password' => 'secret22',

        ]));

        $response->assertInvalid();
    }

    /**
     * @test
     */
    public function validation_login_api_fails_invalid_email()
    {
        $response = $this->post(route('loginAPI', [
            'email' => 'admin2',
            'password' => 'secret22',
        ]));

        $response->assertInvalid();
    }

    /**
     * @test
     */
    public function validation_login_api_fails_invalid_password()
    {
        $response = $this->post(route('loginAPI', [
            'email' => 'admin2@gmail.com',
            'password' => 'se',
        ]));

        $response->assertInvalid();
    }

    /**
     * @test
     */
    public function validation_login_api_fails_invalid_email_and_password()
    {
        $response = $this->post(route('loginAPI', [
            'email' => 'admin2',
            'password' => 'se',
        ]));

        $response->assertInvalid();
    }

    /**
     * @test
     */
    public function Unauthenticated_user_cant_get_data()
    {
        
        $response = $this->post(route('getUserDataAPI'));

        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function Authenticated_user_can_get_data()
    {   
        
        
        // make new user first
        $response = $this->post(route('registerAPI', [
            'name' => 'testapi2',
            'email' => 'initestapi2@gmail.com',
            'password' => 'password'
        ]));

        // $response = $this->post(route('submitcomp', [
        //     'company_name' => 'testcompany',
        //     'address' => 'testaddress',
        //     'city' => 'testtt',
        //     'country' => 'testtt',
        // ]));

        // $response = $this->post(route('submitemplo', [
        //     'name' => 'testemployee',
        //     'password' => 'testemployee',
        //     'company_id' => 1,
        //     'city' => 'jakarta',
        //     'phone' => '081234231234',
        //     'email' => 'employeetest@gmail.com',
        // ]));
            

        // login with new user
        $response = $this->post(route('loginAPI', [
            'email' => 'initestapi2@gmail.com',
            'password' => 'password'
        ]));


        // create token
        $token =  JWTAuth::attempt(['email' => 'initestapi2@gmail.com', 'password' => 'password']);

        // add headers first with token
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json'
        ]) // then try to post route with company_id
        ->post(route('getUserDataAPI', [     
            'company_id' => 1
        ]));

        $response->assertstatus(200);
        // ->assertJson($companies);
        // ->assertJsonStructure([
            
        //     'id',
        //     'name',
        //     'company_name',
        //     'email',
        //     'phone',
        //     'city'  
            

        // ]);
    }

    /**
     * @test
     */
    public function logout_api_with_token()
    {
         // make new user first
         $response = $this->post(route('registerAPI', [
            'name' => 'testapi2',
            'email' => 'initestapi2@gmail.com',
            'password' => 'password'
        ]));

        // login with new user
        $response = $this->post(route('loginAPI', [
            'email' => 'initestapi2@gmail.com',
            'password' => 'password'
        ]));


        // create token
        $token =  JWTAuth::attempt(['email' => 'initestapi2@gmail.com', 'password' => 'password']);

        // add headers first with token
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json'
        ]) // then try to post route with company_id
        ->post(route('logoutAPI'));


        $response->assertJsonStructure([
            'message'
        ])
        ->assertJson([
            'message' => 'Successfully logged out'
        ])
        ->assertStatus(200);
    }
}
