<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\item;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class itemTest extends TestCase
{
    use DatabaseTransactions;

    public $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);
    }
    
    /**
     * @test
     */
    public function user_can_go_to_item_list_page()
    {
        $response = $this->get('/items');

        $response->assertStatus(200);
        
    }

    /**
     * @test
     */
    public function user_can_go_create_item_page()
    {
        $response = $this->get('/inputitems');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_can_submit_item()
    {
        $item = item::factory()->create();
        $response = $this->post(route('submititems'));
        
        $this->assertDatabaseHas('items',[
            'name' => $item->name,
            'price' => $item->price,
        ]);

    }

    /**
     * @test
     */
    public function user_can_go_to_edit_page()
    {
        $item = item::factory()->create();
        $response = $this->get(route('edititems',$item->id));

        $response->assertStatus(200);
    }


    /**
     * @test
     */

    public function user_can_update_the_item()
    {
        $item = item::factory()->create();
        $response = $this->put(route('updateitems',$item->id));
        
        $this->assertDatabaseHas('items',[
            'name' => $item->name,
            'price' => $item->price,
        ]);

    }

    /**
     * @test
     */

    public function user_can_delete_the_item()
    {
        $item = item::factory()->create();
        $response = $this->get(route('deleteitems',$item->id));
        
        $this->assertDatabaseMissing('items',[
            'id' => $item->id,
        ]);

    }
}
