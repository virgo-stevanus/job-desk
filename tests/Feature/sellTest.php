<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\item;
use App\Models\sell;
use App\Models\User;
use App\Models\employee;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class sellTest extends TestCase
{
    use DatabaseTransactions;

    public $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->actingAs($this->user);

        
    }
    
    /**
     * @test
     */
    public function user_can_go_sell_page()
    { 
        $response = $this->get('/sell');

        $response->assertStatus(200);

    }

    /**
     * @test
     */
    public function user_can_go_to_create_sell_form()
    {
        $response = $this->get('/inputsell');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function user_can_submit_sell_form()
    {
        $sell = sell::factory()->create();
        $employee = employee::factory()->create();
        $response = $this->post(route('submitsell'));

        $this->assertDatabaseHas('sells',[
            'created_date' => $sell->created_date,
            'item_id' => $sell->item_id,
            'price' => $sell->price,
            'discount' => $sell->discount,
            'employee_id' => $sell->Semploy->id

        ]);
    }

    /**
     * @test
     */
    public function user_can_go_to_edit_page()
    {
        $sell = sell::factory()->create();
        $response = $this->get(route('editsell', $sell->id));

        $response->assertStatus(200);   
    }

    /**
     * @test
     */

    public function user_can_update_sell()
    {
        $sell = sell::factory()->create();
        $employee = employee::factory()->create();
        $response = $this->put(route('updatesell', $sell->id));

        $this->assertDatabaseHas('sells',[
            'created_date' => $sell->created_date,
            'item_id' => $sell->item_id,
            'price' => $sell->price,
            'discount' => $sell->discount,
            'employee_id' => $sell->Semploy->id

        ]);
    }

    /**
     * @test
     */
    
    public function user_can_delete_sell()
    {
        $sell = sell::factory()->create();
        $response = $this->get(route('deletesell', $sell->id));

        $this->assertDatabaseMissing('sells',[
           'id' => $sell->id,

        ]);

    }

}