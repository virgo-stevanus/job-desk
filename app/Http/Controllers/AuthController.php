<?php

namespace App\Http\Controllers;



use App\Models\User;
use App\Models\employee;
use App\Models\companies;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


// use Illuminate\Validation\Validator;



class AuthController extends Controller
{
    public function getUser()
    {

        $companies = companies::select('*', 'employees.name','employees.company_id','employees.city','employees.phone', 'employees.email')
        ->join('employees', 'employees.company_id', '=', 'companies.id')
        ->select('employees.id as employee_id', 'companies.id as Company_id',  'employees.name', 'companies.company_name', 'employees.email', 'employees.phone', 'employees.city')
        ->get();
        // $companies = companies::select('*', 'employees.name','employees.company_id','employees.city','employees.phone', 'employees.email')
        // ->join('employees', 'employees.company_id', '=', 'companies.id')
        // ->select('companies.id',  'employees.name', 'companies.company_name', 'employees.email', 'employees.phone', 'employees.city')
        // ->get();


        return response()->json([$companies], 200);

    }

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function register (Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
        ]);

        return response()->json([
            'message' => 'user succesfully registered',
            'user' => $user
        ],201);
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|min:6',
        ]);

        if(!$token = JWTAuth::attempt($credentials)){
            return response()->json(['error' => 'Unauthenticated'],401);
           
        }
        return response()->json([
            'status' => true,
            'token' => $token,
            'token_type' => 'bearer',

        ], 200);

    }


    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }
}
