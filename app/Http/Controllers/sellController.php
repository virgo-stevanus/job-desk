<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\item;
use App\Models\sell;
use App\Models\employee;
use Illuminate\Http\Request;
use App\Models\sellsummaries;

class sellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = item::all();
        $employees = employee::all();
        $sells = sell::with('Sitem', 'Semploy')->get();
        return view ('sell', compact('sells', 'items','employees'), [
            'title' => 'Sell',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = item::all();
        $emp = employee::all();
        return view('fitur.create_sell', compact('items', 'emp'), [
            'title' => 'Sell',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'created_date' => 'required',
            'item' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'employee_id' => 'required'
        ]);

    
        $data = new sell;
        $data->created_date = $request->created_date;
        $data->item_id = $request->item;
        $data->price = $request->price;
        $data->discount = $request->discount;
        $data->employee_id = $request->employee_id;
        $data->save();


        $summaries = sellsummaries::where('date','=', Carbon::parse($request->created_date)->format('Y-m-d'))
        ->where('employee_id','=', $request->employee_id)->first();
        $prtotal = empty($summaries) ? 0 : $request->price;
        $pricetotal = $prtotal + $request->price;
        $dsctotal = empty($summaries) ? 0 : $request->discount;
        $discounttotal = $dsctotal + ( $request->discount * $request->price / 100);
        $total = $pricetotal - $discounttotal;

        sellsummaries::updateOrCreate(
            [
                'date' => Carbon::parse($request->created_date)->format('Y-m-d'),
                'employee_id' => $request->employee_id
            ],

            [   
                'price_total' => $pricetotal,
                'discount_total' => $discounttotal,
                'total' => $total,
            ]
        );



        return redirect ('sell');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items= item::all();
        $emp = employee::all();
        $se = sell::with('Sitem', 'Semploy')->findorfail($id);
        return view ('fitur.edit_sell', compact('se','items','emp'), [
            'title' => 'Sell',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'item' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'employee_id' => 'required'
        ]);

        $se = sell::find($id);
        $se->created_date = $request->created_date;
        $se->item_id = $request->item;
        $se->price = $request->price;
        $se->discount = $request->discount;
        $se->employee_id = $request->employee_id;
        $se->update();

        $summaries = sellsummaries::where('date','=', Carbon::parse($request->created_date)->format('Y-m-d'))
        ->where('employee_id','=', $request->employee_id)->first();
        $prtotal = empty($summaries) ? 0 : $request->price;
        $prctotal = $summaries->price_total - $se->price;
        $pricetotal = $prtotal + $prctotal;
        $dsctotal =  empty($summaries) ? 0 : $request->discount;
        $discounttotal = $dsctotal + ( $request->discount * $request->price / 100);
        $disctotal = $summaries->discount_total - $discounttotal;
        $total = $pricetotal - $disctotal;

        sellsummaries::updateOrCreate(
            [
                'date' => Carbon::parse($request->created_date)->format('Y-m-d'),
                'employee_id' => $request->employee_id
            ],

            [   
                'price_total' => $pricetotal,
                'discount_total' => $discounttotal,
                'total' => $total,
            ]
        );

        return redirect('/sell')->with('status', 'Item has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $se = sell::findorfail($id);
        $se->delete();
        return back()->with('status', 'Sell has been deleted');
    }

    public function search(Request $request)
    {
        $items = item::all();
        $employees = employee::all();


        $item_id = request('item_id');
        $price = request('price');
        $discount = request('discount');
        $employee_id = request('employee_id');
        $start_date = request('start_date');
        $end_date = request('end_date');


        $sells = sell::with('Sitem', 'Semploy');
        // $sells = $sells
        $sells = $sells->select('*')
        ->whereHas('Sitem', function($query) use ($item_id) {
            $query->where('item_id', '=', $item_id);
        })
        ->whereHas('SEmploy', function($query) use ($employee_id) {
            $query->where('employee_id', '=', $employee_id);
        })
        ->where('price', '=', $price)
        ->where('discount', '=', $discount)
        ->whereBetween('created_date', [$start_date, $end_date])
        ->get();
        
        return view ('sell', compact('sells','employees','items'), [
            'title' => 'Sell',
        ]);
    }
}
