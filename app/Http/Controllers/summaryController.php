<?php

namespace App\Http\Controllers;

use App\Models\sell;
use App\Models\employee;
use App\Models\companies;
use Illuminate\Http\Request;
use App\Models\sellsummaries;

class summaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comp= companies::all();
        $employees = employee::all();
        $sellsummaries = sellsummaries::with('SSemploy')->get();

        
        return view('summary', compact('sellsummaries', 'comp', 'employees'), [
            'title' => 'Summary',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {   
        $ssum = sellsummaries::with('SSemploy')->findorfail($id);
        $sells = sell::with('Sitem')
        ->whereDate('created_date', '=', $ssum->date)
        ->where('employee_id', '=', $ssum->employee_id)
        ->get();
       
        return view('detailsum.detailsummary', compact ('ssum', 'sells'), [
            'title' => 'Summary',
        ]);
    }

    public function search(Request $request)
    {
        $comp = companies::all();
        $employees = employee::all();

        if($request->start_date && $request->end_date) {
            $sellsummaries = sellsummaries::with('SSemploy', 'SScompanie');
            $sellsummaries = $sellsummaries->whereHas('SSemploy', 
            function ($query) use ($request) 
            {
                $query->where('name', 'like', '%' . $request->employee . '%');
                $getcomp = $request->companies;
                
                $query->whereHas('Rcompany', 
                    function ($query2) use ($getcomp) 
                    {
                        $query2->where('company_name', 'like', '%' . $getcomp . '%');
                    });
            })
            ->whereBetween('date', [$request->start_date, $request->end_date])
            ->get();

        } else {

            $sellsummaries = sellsummaries::with('SSemploy', 'SScompanie');
            $sellsummaries = $sellsummaries->whereHas('SSemploy', 
                function ($query) use ($request) 
                {
                    $query->where('name', 'like', '%' . $request->employee . '%');
                    $getcomp = $request->companies;
                    
                    $query->whereHas('Rcompany', 
                        function ($query2) use ($getcomp) 
                        {
                            $query2->where('company_name', 'like', '%' . $getcomp . '%');
                        });
                })
            ->get();
    
        }
        
        return view('summary', compact('sellsummaries', 'comp', 'employees'), [
            'title' => 'Summary',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
