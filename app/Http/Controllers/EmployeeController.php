<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\employee;
use App\Models\companies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // view all employee data
        $employee = employee::with('Rcompany')->get();
        $companies = companies::all();



        // view to employee page
        return view('employee', compact('employee','companies'),[
            'title' => 'employee',
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $comp = companies::all();
        return view('fitur.create_employee', compact('comp'), [
            'title' => 'employee'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'name' => 'required',
            'password' => 'required',
            'company_id' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);

        $data = new employee;
        $data->name = $request->name;
        $data->password = $request->password;
        $data->company_id = $request->company_id;
        $data->city = $request->city;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->created_users_id = $request->created_users_id;
        $data->updated_users_id = $request->updated_users_id;
        $data->save();

        return redirect ('employee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comp = companies::all();
        $employee = employee::with('Rcompany')->findorfail($id);

        return view('fitur.edit_employee', compact('employee','comp'), [
            'title' => 'employee'
        ]);

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate([
            'name' => 'required',
            'password' => 'required',
            'company_id' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'email' => 'required'
        ]);

        $data = employee::findorfail($id);
        $data->name = $request->name;
        $data->password = $request->password;
        $data->company_id = $request->company_id;
        $data->city = $request->city;
        $data->phone = $request->phone;
        $data->email = $request->email;
        $data->created_users_id = $request->created_users_id;
        $data->updated_users_id = $request->updated_users_id;
        $data->update();

        return redirect ('employee');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delcomp = employee::findorfail($id);
        $delcomp->delete();
        return back();
    }

    public function search(Request $request)
    {
        // $employee = employee::with('Rcompany')->get();
        $companies = companies::all();


        $name = $request->get('name');
        $city = request('city');
        $phone = request('phone');
        $email = request('email');
        $start_date = request('start_date');
        $end_date = request('end_date');


        // $employee = employee::select('*')
        // ->join('companies', 'employees.company_id', '=', 'companies.id')
        // ->where('name', 'LIKE', '%'.$name.'%')
        // ->where('company_id', '=', $request->company)
        // ->get();
        if($request->start_date && $request->$end_date) {
            $employee = employee::with('Rcompany');
            $employee = $employee
            ->whereHas('Rcompany', function($query) use($request){
                $company_id = $request->company;
                $query->where('company_id','=', "$company_id");
            })
            ->where('name', 'like', "%{$name}%")
            ->where('city', 'like', "%{$city}%")
            ->where('phone', 'like', "%" . $phone . "%")
            ->where('email', 'like', "%{$email}%")
            ->whereBetween('created_at', [$start_date, $end_date])
            ->get();
        } else {
            
            $employee = employee::select('*')
            ->where('name', 'like', "%{$name}%")
            ->where('city', 'like', "%{$city}%")
            ->where('phone', 'like', "%" . $phone . "%")
            ->where('email', 'like', "%{$email}%")
            ->whereHas('Rcompany', function($query) use($request){
                $company_id = $request->company;
                $query->where('company_id','=', "$company_id");
            })
            ->get();
        }
        // $employee = $employee
        // ->whereHas('Rcompany', function($query) use($request){
            //     $company_id = $request->company;
            //     $query->where('company_id','=', "$company_id");
            // }) -> get();
        // $employee = employee::with('Rcompany');
        // $employee = $employee->whereHas('Rcompany', function($query) use($request){
        //     $company_id = $request->company;
        //     $query->where('company_id','=', "$company_id");
        // })
        // // ->whereBetween('created_at', [$request->start_date, $request->end_date])
        // ->where('name', 'LIKE', '%' .$name. '%s')
        // ->where('city', 'like', "%" .$city. "%")
        // ->where('phone', 'like', "%" .$phone. "%")
        // ->where('email', 'like', "%".$email."%")
        // ->get();

        return view('employee', compact('employee','companies'),[
            'title' => 'employee',
            
        ]);
    }
}
