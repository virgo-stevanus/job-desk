<?php

namespace App\Http\Controllers;


use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class jobcontroller extends Controller
{
    public function enqueue(Request $request)    
    {
        $details = ['email' =>'recipient@example.com'];
        SendEmail::dispatch($details);
    }
}
