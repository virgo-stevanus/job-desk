<?php

namespace App\Http\Controllers;

use App\Models\item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class itemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
   
    {
        $items = item::all();
        
        return view ('items', compact('items'), [
            'title' => 'Items',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('fitur.create_items');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'items' => 'required',
            'price' => 'required'
        ]);

        $data = new item;
        $data->name = $request->items;
        $data->price = $request->price;
        $data->save();
        return redirect ('items')->with('status', 'Item has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $it = item::findorfail($id);
        return view ('fitur.edit_items', compact('it'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required'
        ]);

        $it = item::find($id);
        $it->name = $request->name;
        $it->price = $request->price;
        $it->save();

        return redirect('/items')->with('status', 'Item has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $it = item::findorfail($id);
        $it->delete();
        return back()->with('status', 'Item has been deleted');
    }

    public function search(Request $request)
    {
        $items = item::all();

        if($request->name)
        {
            $items = item::where('name', 'LIKE', '%'.$request->name.'%')->get();
        }

        if($request->price)
        {
            $items = item::where('price', 'LIKE', '%'.$request->price.'%')->get();
        }

        if($request->name && $request->price)
        {
            $items = item::where('name', 'like', '%'.$request->name.'%')
            ->where('price', 'like', '%'.$request->price.'%')
            ->get();
        }
        return view ('items', compact('items'));
    }   
    
}
