<?php

namespace App\Http\Controllers;


use App\Models\User;
use App\Models\companies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Facade\FlareClient\Http\Response;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $companies = companies::all();

        // view to companies page
        return view('companies', compact('companies'), [
            'title' => 'Companies',
            'active' => 'companies',
            
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::all();
        return view('fitur.create_company', compact('user'),[
            'title' => 'Companies'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'company_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'image' => 'image|file',
            'created_users_id' => 'required',
            'updated_users_id' => 'required',
        ]);
        
        $companies = new Companies;

        $companies->company_name = $request->company_name;
        $companies->address = $request->address;
        $companies->city = $request->city;
        $companies->country = $request->country;
        $companies->image = $request->file('image')->store('images');
        $companies->created_users_id = $request->created_users_id;
        $companies->updated_users_id = $request->updated_users_id;
        $companies->save();
        return redirect('companies')->with('status', 'Company has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = companies::findorfail($id);
        return view('fitur.edit_company', compact('companies'), [
            'title' => 'Companies'
        ]);

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request -> validate([
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'image' => 'image|file',
            'created_users_id' => 'required',
            'updated_users_id' => 'required',
        ]);

        $companies = companies::findorfail($id);
        $companies->company_name = $request->name;
        $companies->address = $request->address;
        $companies->city = $request->city;
        $companies->country = $request->country;
        $companies->image = $request->file('image')->store('images');
        $companies->created_users_id = $request->created_users_id;
        $companies->updated_users_id = $request->updated_users_id;
        $companies->update();

        return redirect('companies')->with('status', 'Company has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // make delete with $id parameter
        $delcomp = companies::findorfail($id);
        $delcomp->delete();
        return back();
    }

    public function search(Request $request)
    {   
        $companies = companies::all();


        $name = request('company_name');
        $address = request('address');
        $city = request('city');
        $country = request('country');
        $start_date = request('start_date');
        $end_date = request('end_date');


        if($request->start_date && $request->end_date){
            $companies = companies::whereBetween('created_at', [$start_date, $end_date])->get();
            $companies = companies::select('*')->where('company_name', 'LIKE', '%'.$name.'%')
            ->where('address', 'LIKE', '%'.$address.'%')
            ->where('city', 'LIKE', '%'.$city.'%')
            ->where('country', 'LIKE', '%'.$country.'%')
            ->get();

        } else {

            $companies = companies::select('*')->where('company_name', 'LIKE', '%'.$name.'%')
            ->where('address', 'LIKE', '%'.$address.'%')
            ->where('city', 'LIKE', '%'.$city.'%')
            ->where('country', 'LIKE', '%'.$country.'%')
            ->get();
        }


        return view('companies', compact('companies'), [
            'title' => 'Companies'
        ]);
    }

   
}
