<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class sellsummaries extends Model
{
    use HasFactory;
    protected $table = 'sellsummaries';
    protected $fillable = [
        'date',
        'employee_id',
        'price_total',
        'discount_total',
        'total'
    ];

    public function SSemploy()
    {
        return $this->belongsTo(employee::class, 'employee_id')->withDefault();
    }

    public function SScompanie()
    {
        return $this->belongsTo(companies::class);
    }
    public function getCreatedAtAttribute($value)
    {
       return Carbon::createFromTimestamp(strtotime($value))
        ->timezone(Session::get('tz'))
        ->toDateTimeString();
    }
    
    public function getUpdatedAtAttribute($value)
    {
       return Carbon::createFromTimestamp(strtotime($value))
        ->timezone(Session::get('tz'))
        ->toDateTimeString();
    }

}
