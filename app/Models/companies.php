<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class companies extends Model
{
    use HasFactory;
    // make protected 
    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $fillable = [
        'company_name',
        'address',
        'city',
        'created_users_id',
        'updated_users_id',
        'country'
    ];

    public function Remployee()
    {
        return $this->hasMany(employee::class);
    }
    
    public function getCreatedAtAttribute($value)
    {
       return Carbon::createFromTimestamp(strtotime($value))
        ->timezone(Session::get('tz'))
        ->toDateTimeString();
    }
    
    public function getUpdatedAtAttribute($value)
    {
       return Carbon::createFromTimestamp(strtotime($value))
        ->timezone(Session::get('tz'))
        ->toDateTimeString();
    }

   

}
