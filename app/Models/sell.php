<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\item;
use App\Models\employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class sell extends Model
{
    use HasFactory;
    protected $table = 'sells';
    protected $primaryKey = 'id';
    protected $fillable = [
        'created_date',
        'item_id',
        'price',
        'discount',
        'employee_id',
    ];

    public function Sitem()
    {
        return $this->belongsTo(item::class, 'item_id');
    }

    public function Semploy()
    {
        return $this->belongsTo(employee::class, 'employee_id')->withDefault();
    }

    // public function getCreatedDateAttribute($value)
    // {
    //    return Carbon::createFromTimestamp(strtotime($value))
    //     ->timezone(Session::get('tz'))
    //     ->toDateTimeString();
    // }
}
