<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class employee extends Model
{
    use HasFactory;
    // make protected
    protected $table = 'employees';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'password',
        'company_id',
        'city',
        'phone',
        'email',
        'created_users_id',
        'updated_users_id',
       
    ];

    public function Rcompany()
    {
        return $this->belongsTo(companies::class, 'company_id')->withDefault();
    }

    public function Rsell()
    {
        return $this->hasMany(sell::class);
    }

    public function Rsummaries()
    {
        return $this->hasMany(sellsummaries::class);
    }

    public function getCreatedAtAttribute($value)
    {
       return Carbon::createFromTimestamp(strtotime($value))
        ->timezone(Session::get('tz'))
        ->toDateTimeString();
    }
    
    public function getUpdatedAtAttribute($value)
    {
       return Carbon::createFromTimestamp(strtotime($value))
        ->timezone(Session::get('tz'))
        ->toDateTimeString();
    }


    
   
   
}
