<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesController;
use App\Htpp\Controllers\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DataController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register'])->name('registerAPI');
Route::post('/login', [AuthController::class, 'login'])->name('loginAPI');

Route::group(['middleware' => 'api'], function(){
    Route::post('/user', [AuthController::class, 'getUser'])->name('getUserDataAPI');
    Route::post('/logout', [AuthController::class, 'logout'])->name('logoutAPI');

});