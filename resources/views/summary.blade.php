@extends('layouts.main')

@section('lyts')

<?php 
use Carbon\Carbon;
?>
<h2>{{ trans('companies.listsummary') }}</h2>

{{-- search --}}
<form action="{{ route('searchsummary') }}" method="GET">

  <input list="comp" id="mys3"  placeholder="Companies" name="companies">
  <datalist id="comp">
    @foreach($comp as $cp)
    <option value="{{ $cp->company_name}}" {{ request('companies') == $cp->company_name ? 'selected' : NULL }}>{{ $cp->company_name}}</option>
    @endforeach
  </datalist>
  <input list="emp" id="mys4"  placeholder="Employee" name="employee">
  <datalist id="emp">
    @foreach($employees as $emp)
    <option value="{{ $emp->name }}" {{ request('employee') == $emp->name ? 'selected' : NULL }}>{{ $emp->name }}</option>
    @endforeach
  </datalist>
  <input type="date" id="mys5"  name="start_date" value="{{ request('start_date') }}">
  <input type="date" id="mys6"  name="end_date" value="{{ request('end_date') }}">
  <button class="btn btn-primary" type="submit">Search</button>
  
</form>
{{-- end search --}}


{{-- TABLE CONTENT --}}
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-bordered table-hover dt-responsive text-center">
        <thead>
          <tr>
            <th class="bg-primary text-center" >{{ trans('companies.date') }}</th>
            <th class="bg-primary text-center" >{{ trans('companies.company') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.employe') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.createdat') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.lastupdate') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.pricetotal') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.discounttotal') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.total') }}</th>
          
          </tr>
        </thead>
        <tbody>
            @foreach($sellsummaries as $ssm)
            <tr>
                <td><a href="{{ route('detailsummary', $ssm->id) }}">{{ Carbon::parse($ssm->date)->format('Y-m-d')}} </a></td>
                <td>{{ $ssm->SSemploy->Rcompany->company_name}}</td>
                <td>{{ $ssm->SSemploy->name }}</td>
                <td>{{ $ssm->created_at}}</td>
                <td>{{ $ssm->updated_at }}</td>
                <td>{{ number_format( $ssm->price_total) }}</td>
                <td>{{ number_format($ssm->discount_total) }}</td>
                <td>{{ number_format($ssm->total) }}</td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>
<script src='https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js'></script>
<script  src="../js/script.js"></script>
@endsection