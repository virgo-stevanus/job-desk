@extends('layouts.main')

@section('lyts')
<h2>{{ trans('companies.listcompany') }}</h2>

{{-- CRUD DROPDOWN --}}
<div class="dropdown" aria-labelledby="navbarDropdown" style="float:right; ">
  <button class="dropbtn"><h4>+</h4></button>
  <div class="dropdown-content  dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float:right; oveflow: auto; max-heiht:200px;">
    <a href="/inputcompanies" class="dropdown-item" >{{ trans('companies.company') }}</a>
    <a href="/inputemployee" class="dropdown-item" >{{ trans('companies.employe') }}</a>
    <a href="/inputitems" class="dropdown-item" >{{ trans('companies.items') }}</a>
    <a href="/inputsell" class="dropdown-item" >{{ trans('companies.sell') }}</a>
  </div>
</div>
{{-- END CRUD DROPDOWN --}}

{{-- Search --}}
<form action="{{ route('searchcomp') }}" method="GET">
 
  <input type="search" id="myInput"  placeholder="Name" name="company_name" value="{{ request('company_name') }}">
  <input type="search" id="myInput2"  placeholder="Address" name="address" value="{{ request('address') }}">
  <input type="search" id="myInput3"  placeholder="City" name="city" value="{{ request('city') }}">
  <input type="search" id="myInput4"  placeholder="Country" name="country" value="{{ request('country') }}">
  <input type="date" id="myInput5"  name="start_date" value="{{ request('start_date') }}">
  <input type="date" id="myInput6"  name="end_date" value="{{ request('end_date') }}">
  <button class="btn btn-primary" type="submit">Search</button>
  
</form>
{{-- END SEARCH --}}


{{-- TABLE CONTENT --}}
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-bordered table-hover dt-responsive text-center" id="myTable" >
        <thead>
          <tr>
            <th class="bg-primary text-center" >{{ trans('companies.name') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.address') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.city') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.country') }}</th>
            <th class="bg-primary text-center">Logo</th>
            <th class="bg-primary text-center">{{ trans('companies.createdat') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.updatedat') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.action') }}</th>
          </tr>
        </thead>
        <tbody>
            @foreach($companies as $cps)
            <tr>
                <td>{{ $cps->company_name }}</td>
                <td>{{ $cps->address }}</td>
                <td>{{ $cps->city }}</td>
                <td>{{ $cps->country }}</td>
                @if ($cps->image)
                  <td><img src="{{ asset('storage/'.$cps->image) }}" alt="" style="max-height: 100px"></td>                    
                @else
                  <td>-</td>
                @endif
                <td>{{ $cps->created_at}}</td>
                <td>{{ $cps->updated_at }}</td>
                <td>
                  <a href="{{ route('editcomp', $cps->id) }}" class="btn btn-success p-1 mb-1">{{ trans('companies.edit') }}</a>  | <a href="{{ route('deletecomp', $cps->id) }}" class="btn btn-warning">{{ trans('companies.delete') }} </a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>
<script src='https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js'></script>
<script  src="../js/script.js"></script>
@endsection