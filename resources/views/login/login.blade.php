<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login | Virgo stevanus</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/login.css">
</head>
<body>
  <div class=" flex-r container">
    <div class="flex-r login-wrapper">
      <div class="login-text">
        <div class="logo">
          <span><i class="fab fa-speakap"></i></span>
          <span>CRM System</span>
        </div>
        <h1>Sign in</h1>
        <p>It's not long before you embark on this journey! </p>

        <form class="flex-c" action="{{ route('loginuser') }}" method="post">
            {{ csrf_field() }}
            @if (Session::has('loginerror'))
                <span class="extra-line">
                    {{ Session::get('loginerror') }}
                </span>
            @endif

            <div class="input-box">
                <span class="label">E-mail</span>
                <div class=" flex-r input">
                    <input type="text" placeholder="name@abc.com" id="email" name="email"  @error('email') is-invalid @enderror value="{{ old('email') }}" required>
                    <i class="fas fa-at"></i>
                </div>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span> 
                @enderror
            </div>

            <div class="input-box">
                <span class="label">Password</span>
                <div class="flex-r input">
                    <input type="password" placeholder="8+ (a, A, 1, #)" id="password" name="password"  required @error('password') is-invalid @enderror>
                    <i class="fas fa-lock"></i>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span> 
                 @enderror
            </div>
            <button class="btn extra-line" type="submit">Login</button>          
          
        </form>

      </div>
    </div>
  </div>
</body>

</html>