<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


{{-- Form CSS --}}
<link rel="stylesheet" href="../css/form-mini.css">
{{-- <link rel="stylesheet" href="../css/demo.css"> --}}

{{-- Navbar CSS --}}
<link rel="stylesheet" href="../css/navbar.css">

{{-- Drop down TIME --}}
<link rel="stylesheet" href="../css/dropdown.css">

{{-- Drop down Language --}}
<link rel="stylesheet" href="../css/dropdownlang.css">


<title>Hello, world!</title>
</head>

<body>
{{-- NAVBAR --}}
<header class="hdr">

  {{-- Title --}}
  <h1 class="tls">CRM<span class="sys"> System</span></h1>
  {{-- Title --}}

  {{-- Logout Button --}}
  <form class="flogout" action="{{ route('logout') }}" method="post">
    {{ csrf_field() }}
    <button class="tombol" type="submit"><span class="Bagiana">Logout</span></button> 
  </form>
  {{-- Logout Button --}}

  {{-- Nav link --}}
  <nav>
    <ul class="AtasUL">
      <li class="IniLI"><a href="/home" class="Bagiana">{{ trans('companies.home') }}</a></li>
      <li class="IniLI"><a href="/companies" class="Bagiana">{{ trans('companies.company') }}</a></li>
      <li class="IniLI"><a href="/employee" class="Bagiana">{{ trans('companies.employe') }}</a></li>
      <li class="IniLI"><a href="/items" class="Bagiana">{{ trans('companies.items') }}</a></li>
      <li class="IniLI"><a href="/sell" class="Bagiana">{{ trans('companies.sell') }}</a></li>
      <li class="IniLI"><a href="/summary" class="Bagiana">{{ trans('companies.summary') }}</a></li>
    </ul>
  </nav>
  {{-- END Nav Link --}}
</header>

{{-- DropDown Languange --}}
<div class="droplang">
  <button class="dropbtnlang">{{ trans('companies.lang') }}</button>
  <div class="droplang-content">
    <a href="{{ url('locale/id') }}" >{{ trans('companies.indonesia')}}</a>
    <a href="{{ url('locale/en') }}" >{{ trans('companies.english')}}</a>
  </div>
</div> {{-- END DROPDOWN language --}}

{{-- END NAVBAR --}}



{{-- CONTENT --}}
<div>
  @yield('formcss')
</div>
{{-- END CONTENT --}}


</body>
@yield('script') {{-- JS Script --}}
</html>