<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

{{-- DATA TABLES  CSS--}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.css'>
<link rel='stylesheet' href='https://cdn.datatables.net/responsive/1.0.4/css/dataTables.responsive.css'>

{{-- Data tables --}}
<link rel="stylesheet" href="../css/style.css">

{{-- Navbar CSS --}}
<link rel="stylesheet" href="../css/navbar.css">

{{-- Drop down TIME --}}
<link rel="stylesheet" href="../css/dropdown.css?v=0.3">

{{-- Drop down Language --}}
<link rel="stylesheet" href="../css/dropdownlang.css">

{{-- search css --}}
<link rel="stylesheet" href="../css/search.css?v=0.3">


<title>Hello, world!</title>
</head>

<body>
  {{-- NAVBAR --}}
<header class="hdr">
  {{-- Drop Down Time --}}
  <div class="dropdown" aria-labelledby="navbarDropdown" style="float: left; ">
      <button class="dropbtn">{{ trans('companies.time') }}</button>
      <div class="dropdown-content dropdown-menu dropdown-menu-left">
        @foreach(timezone_identifiers_list() as $timezone)
          <a href="{{ route('tz.switch', str_replace('/',' ', $timezone))}}">{{ $timezone }}</a>
        @endforeach
      </div>
  </div>
  {{-- End Dropdown Time --}}


  {{-- Title --}}
  <h1 class="tls">CRM<span class="sys"> System</span></h1>
  {{-- Title --}}

  {{-- Logout Button --}}
  <form class="flogout" action="{{ route('logout') }}" method="post">
    {{ csrf_field() }}
    <button class="tombol" type="submit"><span class="Bagiana">Logout</span></button> 
  </form>
  {{-- Logout Button --}}

  {{-- Nav link --}}
  <nav>
    <ul class="AtasUL">
      <li class="IniLI"><a href="/home" class="Bagiana">{{ trans('companies.home') }}</a></li>
      <li class="IniLI"><a href="/companies" class="Bagiana">{{ trans('companies.company') }}</a></li>
      <li class="IniLI"><a href="/employee" class="Bagiana">{{ trans('companies.employe') }}</a></li>
      <li class="IniLI"><a href="/items" class="Bagiana">{{ trans('companies.items') }}</a></li>
      <li class="IniLI"><a href="/sell" class="Bagiana">{{ trans('companies.sell') }}</a></li>
      <li class="IniLI"><a href="/summary" class="Bagiana">{{ trans('companies.summary') }}</a></li>
    </ul>
  </nav>
  {{-- END Nav Link --}}
</header>

{{-- DropDown Languange --}}
<div class="droplang">
    <button class="dropbtnlang">{{ trans('companies.lang') }}</button>
    <div class="droplang-content">
      <a href="{{ url('locale/id') }}" >{{ trans('companies.indonesia')}}</a>
      <a href="{{ url('locale/en') }}" >{{ trans('companies.english')}}</a>
    </div>
</div> {{-- END DROPDOWN language --}}

{{-- END NAVBAR --}}


{{-- CONTENT --}}
<div>
  @yield('lyts')
</div>
{{-- END CONTENT --}}


</body>
</html>