@extends('layouts.mainsum')

@section('lyts')

<?php 
use Carbon\Carbon;

?>

<h2 style="text-align: center">{{ trans('companies.detailsummary') }}</h2>
<table >
    
    <thead >
      
     
      <input type="text" id="myds"  placeholder="Date"  readonly value="{{ $ssum->date }}">
      <input type="text" id="myds2"  placeholder="Company"  value="{{ $ssum->SSemploy->Rcompany->company_name }}" readonly>
      <input type="text" id="myds3"  placeholder="Employee"  value="{{ $ssum->SSemploy->name}}" readonly>
      <tr >
        <th scope="col">{{ trans('companies.No') }}</th>
        <th scope="col">{{ trans('companies.items') }}</th>
        <th scope="col">{{ trans('companies.price') }}</th>
        <th scope="col">{{ trans('companies.discount') }} (%)</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        $i = 1;
      ?>
      @foreach($sells as $sell)
      <tr>
        <td >{{ $i++ }}</td>
        <td >{{ $sell->Sitem->name }}</td>
        <td >{{ number_format($sell->price) }}</td>
        <td >{{ $sell->discount }}</td>
      </tr>
      @endforeach
    </tbody>
    <tfoot>
    </tfoot>
</table>
@endsection