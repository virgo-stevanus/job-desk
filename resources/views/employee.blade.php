@extends('layouts.main')

@section('lyts')
<h2>{{ trans('companies.listemploye') }}</h2>

{{-- CRUD DROPDOWN --}}
<div class="dropdown" aria-labelledby="navbarDropdown" style="float:right; ">
  <button class="dropbtn"><h4>+</h4></button>
  <div class="dropdown-content  dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float:right; oveflow: auto; max-heiht:200px;">
    <a href="/inputcompanies" class="dropdown-item" >{{ trans('companies.company') }}</a>
    <a href="/inputemployee" class="dropdown-item" >{{ trans('companies.employe') }}</a>
    <a href="/inputitems" class="dropdown-item" >{{ trans('companies.items') }}</a>
    <a href="/inputsell" class="dropdown-item" >{{ trans('companies.sell') }}</a>
  </div>
</div>
{{-- END CRUD DROPDOWN --}}

{{-- search --}}
<form action="{{ route("searchemplo") }}" method="GET">
  @csrf
  <input type="text" id="myem"  placeholder="Name" name="name" value="{{ request('name') }}">
  <select  id="myem2" name="company" >
    <option value=""> Select Company</option>
    @foreach ($companies as $cps)
      <option value="{{ $cps->id}}" {{ request('company') == $cps->id ? 'selected' : null }}>{{ $cps->company_name }}</option>
    @endforeach
  </select>
  <input type="text" id="myem3"  placeholder="City" name="city" value="{{ request('city') }}">
  <input type="text" id="myem4"  placeholder="Phone" name="phone" value="{{ request('phone') }}">
  <input type="text" id="myem2"  placeholder="Email" name="email" value="{{ request('email') }}">
  <input type="date" id="myem5"  name="start_date" value="{{ request('start_date') }}">
  <input type="date" id="myem6"  name="end_date" value="{{ request('end_date') }}">
  <button class="btn btn-primary" type="submit">Search</button>
  
</form>
{{-- end search --}}


{{-- TABLE CONTENT --}}
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-bordered table-hover dt-responsive text-center">
        <thead>
          <tr>
            <th class="bg-primary text-center">{{ trans('companies.name') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.company') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.city') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.phone') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.email') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.createdat') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.updatedat') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.action') }}</th>
          </tr>
        </thead>
        <tbody>
            @foreach($employee as $emp)
            <tr>
                <td>{{ $emp->name }}</td>
                <td>{{ $emp->Rcompany->company_name }}</td>
                <td>{{ $emp->city }}</td>
                <td>{{ $emp->phone }}</td>
                <td>{{ $emp->email }}</td>
                <td>{{ $emp->created_at }}</td>
                <td>{{ $emp->updated_at }}</td>
                
                <td>
                    <a href="{{ route('editemplo', $emp->id) }}" class="btn btn-success p-1 mb-1">{{ trans('companies.edit') }}</a>  | <a href="{{ route('deleteemplo', $emp->id) }}" class="btn btn-warning">{{ trans('companies.delete') }}</a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>
<script src='https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js'></script>
<script  src="../js/script.js"></script>
@endsection