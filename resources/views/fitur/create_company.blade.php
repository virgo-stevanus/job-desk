@extends('layouts.mainform')

@section('formcss')
<div class="main-content">

    <div class="form-mini-container">
      <h1 style="text-align: center">{{ trans('companies.inputcompany') }}</h1>

      <form class="form-mini" method="post" action="{{ route('submitcomp') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-row">
            <label for="name"><h3>{{ trans('companies.name') }}</h3></label>
            <input type="text" id="name" name="company_name" placeholder="Name" required>
        </div>
        <div class="form-row">
            <label for="address"><h3>{{ trans('companies.address') }}</h3></label>
            <input type="text" id="address" name="address" placeholder="Address" required>
        </div>
        <div class="form-row">
            <label for="city"><h3>{{ trans('companies.city') }}</h3></label>
            <input type="text" id="city" name="city" placeholder="City" required>
        </div>
        <div class="form-row">
            <label for="image"><h3>Logo</h3></label>
            <input type="file" id="image" name="image" placeholder="image" class="@error('image') is-invalid @enderror">
        </div>
        @error('image')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror

        <div class="form-row">
            <label for="country"><h3>{{ trans('companies.country') }}</h3></label>
            <input type="text" id="country" name="country" placeholder="country" required>
        </div>
        <div class="form-row">
            <input type="text" name="created_users_id" hidden value="{{ Auth::id() }}">
        </div>
        <div class="form-row">
            <input type="text" name="updated_users_id" hidden value="{{ Auth::id() }}">
        </div>

        <div class="form-row form-last-row">
            <button type="submit">{{ trans('companies.submit') }}</button>
        </div>
      </form>
  </div>

</div>
@endsection