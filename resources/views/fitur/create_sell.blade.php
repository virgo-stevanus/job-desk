@extends('layouts.mainform')

@section('formcss')

<?php 
use Carbon\Carbon;
?>


<div class="main-content">
    <div class="form-mini-container item-select">
        <h1 style="text-align: center">{{ trans('companies.inputsell') }}</h1>
        
        <form class="form-mini" method="post" action="{{ route('submitsell') }}" enctype="multipart/form-data" >
            {{ csrf_field() }}
            <input type="text" name="created_date" placeholder="date" id="created_date" value="{{ Carbon::now()->format('Y-m-d H:i'); }}" hidden>
            <div class="form-row ">
                <label>
                    <label for="item">{{ trans('companies.items') }}</label>
                    <select name="item" id="item" value="{{ old('item') }}">
                        @foreach ($items as $item)
                            <option value="{{ $item->id }}" data-price="{{ $item->price }}" >{{ $item->name }}</option>
                        @endforeach
                    </select>  
                </label>
            </div>

            <div class="form-row">
                <label for="price">{{ trans('companies.price') }}</label>
                <input type="number" name="price" placeholder="Price" readonly class="price-input">
            </div>
            <div class="form-row">
                <label for="discount">{{ trans('companies.discount') }}(%)</label>
                <input type="number" name="discount" placeholder="Discount" id="discount" required>
            </div>
            <div class="form-row">
                <label>
                <label for="employee_id">{{ trans('companies.employe') }}</label>
                    <select name="employee_id" id="employee_id" value="{{ old('employee') }}">
                        @foreach ($emp as $ep)    
                            <option value="{{ $ep->id }}" >{{ $ep->name }}</option>
                        @endforeach
                    </select>
                </label>
            </div>
            <div class="form-row form-last-row">
                <button type="submit">{{ trans('companies.submit') }}</button>
            </div>
        </form>
    </div>

</div>
@endsection

@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    $('.item-select').on('change', function(){
        $('.price-input')
            .val(
                $(this).find(':selected').data('price')
            );
    });
</script>
@endsection