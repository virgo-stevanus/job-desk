@extends('layouts.mainform')

@section('formcss')
<div class="main-content">

    <div class="form-mini-container">
      <h1 style="text-align: center">{{ trans('companies.inputcompany') }}</h1>

      <form class="form-mini" method="post" action="{{ route('updatecomp', $companies->id) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        @method('PUT')
        <div class="form-row">
            <label for="name"><h3>{{ trans('companies.name') }}</h3></label>
            <input type="text" id="name" name="name" placeholder="Name" required value="{{ $companies->company_name }}">
        </div>
        <div class="form-row">
            <label for="address"><h3>{{ trans('companies.address') }}</h3></label>
            <input type="text" id="address" name="address" placeholder="Address" required value="{{ old('address', $companies->address) }}">
        </div>
        <div class="form-row">
            <label for="city"><h3>{{ trans('companies.city') }}</h3></label>
            <input type="text" id="city" name="city" placeholder="City" required value="{{ old('city', $companies->city) }}">
        </div>
        <div class="form-row">
            <label for="image"><h3>{{ trans('companies.image') }}</h3></label>
            <input type="file" id="image" name="image" placeholder="image" class="@error('image') is-invalid @enderror" value="{{ $companies->image }}">
        </div>
        @error('image')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
        @enderror
        <div class="form-row">
            <label for="country"><h3>Logo</h3></label>
            <input type="text" id="country" name="country" placeholder="Country" required value="{{ old('country', $companies->country) }}">
        </div> 
        <div class="form-row">
            <input type="text" name="created_users_id" hidden value="{{ Auth::id() }}" >
        </div>
        <div class="form-row">
            <input type="text" name="updated_users_id" hidden value="{{ Auth::id() }}">
        </div>

        <div class="form-row form-last-row">
            <button type="submit">{{ trans('companies.submit') }}</button>
        </div>
      </form>
  </div>

</div>
@endsection