@extends('layouts.main')

@section('lyts')
<h2>{{ trans('companies.listsell') }}</h2>

{{-- CRUD DROPDOWN --}}
<div class="dropdown" aria-labelledby="navbarDropdown" style="float:right; ">
  <button class="dropbtn"><h4>+</h4></button>
  <div class="dropdown-content  dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown" style="float:right; oveflow: auto; max-heiht:200px;">
    <a href="/inputcompanies" class="dropdown-item" >{{ trans('companies.company') }}</a>
    <a href="/inputemployee" class="dropdown-item" >{{ trans('companies.employe') }}</a>
    <a href="/inputitems" class="dropdown-item" >{{ trans('companies.items') }}</a>
    <a href="/inputsell" class="dropdown-item" >{{ trans('companies.sell') }}</a>
  </div>
</div>
{{-- END CRUD DROPDOWN --}}

{{-- search --}}
<form action="{{ route("searchsell") }}" method="GET">

  <select  id="mysell" name="item_id" >
    <option value="">Select Items</option>
    @foreach ($items as $item)
      <option value="{{ $item->id}}" {{ request('item_id') == $item->id ? 'selected' : NULL }}>{{ $item->name }}</option>
    @endforeach
  </select>
  <input type="search" id="mysell2"  placeholder="Price" name="price" value="{{ request('price') }}">
  <input type="search" id="mysell3"  placeholder="Discount (%)" name="discount" value="{{ request('discount') }}">
  <select  id="mysell4" name="employee_id" >
    <option value="">Select Employee</option>
    @foreach ($employees as $ep)
    <option value="{{ $ep->id}}" {{ request('employee_id') == $ep->id ? 'selected' : NULL }}>{{ $ep->name }}</option>
    @endforeach
  </select>
  <input type="date" id="mysell5"  name="start_date" value="{{ request('start_date') }}">
  <input type="date" id="mysell6"  name="end_date" value="{{ request('end_date') }}">
  <button class="btn btn-primary" type="submit">Search</button>
  
</form>
{{-- end search --}}

{{-- TABLE CONTENT --}}
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-bordered table-hover dt-responsive text-center">
        <thead>
          <tr>
            <th class="bg-primary text-center" >{{ trans('companies.date') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.name') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.price') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.discount') }} (%)</th>
            <th class="bg-primary text-center">{{ trans('companies.employe') }}</th>
            <th class="bg-primary text-center">{{ trans('companies.action') }}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($sells as $sell)
            <tr>
              <td>{{ $sell->created_date }}</td>
              <td>{{ $sell->Sitem->name }}</td>
              <td>{{ number_format($sell->price) }}</td>
              <td>{{ $sell->discount }}</td>
              <td>{{ $sell->Semploy->name }}</td>
              <td>
                <a href="{{ route('editsell',$sell->id) }}" class="btn btn-success p-1 mb-1">{{ trans('companies.edit') }}</a>  | <a href="{{ route('deletesell', $sell->id) }}" class="btn btn-warning">{{ trans('companies.delete') }} </a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdn.datatables.net/1.10.5/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/plug-ins/f2c75b7247b/integration/bootstrap/3/dataTables.bootstrap.js'></script>
<script src='https://cdn.datatables.net/responsive/1.0.4/js/dataTables.responsive.js'></script>
<script  src="../js/script.js"></script>
@endsection