<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password')->nullable()->change();

        });

        // make a foreign key for table companies to user
        Schema::table('companies', function (Blueprint $table) {
            $table->bigInteger('created_users_id')->unsigned()->nullable();
            $table->bigInteger('updated_users_id')->unsigned()->nullable();
            $table->foreign('created_users_id')->references('id')->on('users');
            $table->foreign('updated_users_id')->references('id')->on('users');
        });

        // make foreign key for table employee to user
        Schema::table('employees', function (Blueprint $table) {
            $table->bigInteger('created_users_id')->unsigned()->nullable();
            $table->bigInteger('updated_users_id')->unsigned()->nullable();
            $table->foreign('created_users_id')->references('id')->on('users');
            $table->foreign('updated_users_id')->references('id')->on('users');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
