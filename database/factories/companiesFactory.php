<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class companiesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => 'airfast',
            'address' => 'jakarta',
            'city' => 'jakart',
            'country' => 'indonesia',
            'created_users_id' => 1,
            'updated_users_id' => 1,
        ];
    }
}
