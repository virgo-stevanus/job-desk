<?php

namespace Database\Seeders;

// use models user


use App\Models\User;
use Faker\Provider\ar_EG\Company;
use Illuminate\Database\Seeder;
use Spatie\TranslationLoader\LanguageLine;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         // \App\Models\User::factory(10)->create();
         User::create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);

        $this->call(CompanySeeder::class);
        $this->call(EmployeeSeeder::class);
        $this->call(itemSeeder::class);
        $this->call(sellSeeder::class);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'home',
            'text' => ['en' => 'Home', 'id' => 'Beranda'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'company',
            'text' => ['en' => 'Companies', 'id' => 'Perusahaan'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'employe',
            'text' => ['en' => 'Employee', 'id' => 'Karyawan'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'login',
            'text' => ['en' => 'Login', 'id' => 'Masuk'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'listcompany',
            'text' => ['en' => 'List Company', 'id' => 'Daftar Perusahaan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'listemploye',
            'text' => ['en' => 'List Employee', 'id' => 'Daftar Karyawan'],
        ]); 
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'name',
            'text' => ['en' => 'Name', 'id' => 'Nama'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'address',
            'text' => ['en' => 'Address', 'id' => 'Alamat'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'city',
            'text' => ['en' => 'City', 'id' => 'Kota'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'country',
            'text' => ['en' => 'Country', 'id' => 'Negara'], 
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'createdat',
            'text' => ['en' => 'Created Date', 'id' => 'Dibuat Tanggal'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'updatedat',
            'text' => ['en' => 'Updated Date', 'id' => 'Diubah Tanggal'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'phone',
            'text' => ['en' => 'Phone', 'id' => 'Telepon'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'email',
            'text' => ['en' => 'Email', 'id' => 'Surel'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'time',
            'text' => ['en' => 'Time', 'id' => 'Waktu'],
        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'action',
            'text' => ['en' => 'Action', 'id' => 'Aksi'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'indonesia',
            'text' => ['en' => 'Indonesia', 'id' => 'Indonesia'],

        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'english',
            'text' => ['en' => 'English', 'id' => 'English'],

        ]);
        
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'lang',
            'text' => ['en' => 'Language', 'id' => 'Bahasa'],

        ]);
        
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'edit',
            'text' => ['en' => 'Edit', 'id' => 'Ubah'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'delete',
            'text' => ['en' => 'Delete', 'id' => 'Hapus'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'items',
            'text' => ['en' => 'Items', 'id' => 'Barang'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'sell',
            'text' => ['en' => 'Sell', 'id' => 'Jual'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'summary',
            'text' => ['en' => 'Summary', 'id' => 'Ringkasan Penjualan'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'listitems',
            'text' => ['en' => 'List Items', 'id' => 'Daftar Barang'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'listsell',
            'text' => ['en' => 'List Sell', 'id' => 'Daftar Penjualan'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'listsummary',
            'text' => ['en' => 'List Summary', 'id' => 'Daftar Ringkasan Penjualan'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'price',
            'text' => ['en' => 'Price', 'id' => 'Harga'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'listsell',
            'text' => ['en' => 'List Sell', 'id' => 'Daftar Penjualan'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'listsell',
            'text' => ['en' => 'List Sell', 'id' => 'Daftar Penjualan'],

        ]);
        
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'date',
            'text' => ['en' => 'Date', 'id' => 'Tanggal'],

        ]);

        
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'submit',
            'text' => ['en' => 'Submit', 'id' => 'Kirim'],

        ]);
        
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'update',
            'text' => ['en' => 'Update', 'id' => 'Perbarui'],

        ]);
        
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'edititems',
            'text' => ['en' => 'Edit Items', 'id' => 'Ubah Barang'],

        ]);
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'editsell',
            'text' => ['en' => 'Edit Sell', 'id' => 'Ubah Penjualan'],

        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'inputitems',
            'text' => ['en' => 'Input Items', 'id' => 'Buat Barang'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'inputsell',
            'text' => ['en' => 'Input Sell', 'id' => 'Buat Penjualan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'discount',
            'text' => ['en' => 'Discount', 'id' => 'Potongan'],
        ]);
        
        Languageline::create([
            'group' => 'companies',
            'key' => 'lastupdate',
            'text' => ['en' => 'Last Update', 'id' => 'Terakhir Diubah'],
        ]);
        
     
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'pricetotal',
            'text' => ['en' => 'Price Total', 'id' => 'Total Harga'],
        ]);

        
        LanguageLine::create([
            'group' => 'companies',
            'key' => 'discounttotal',
            'text' => ['en' => 'Discount Total', 'id' => 'Total Potongan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'inputcompany',
            'text' => ['en' => 'Input Companies', 'id' => 'Buat Company'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'inputemployee',
            'text' => ['en' => 'Input Employee', 'id' => 'Buat Karyawan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'total',
            'text' => ['en' => 'Total', 'id' => 'Total'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'detailsummary',
            'text' => ['en' => 'Detail Summary', 'id' => 'Detail Ringkasan'],
        ]);

        LanguageLine::create([
            'group' => 'companies',
            'key' => 'No',
            'text' => ['en' => 'No', 'id' => 'Nomor'],
        ]);
    }


}
