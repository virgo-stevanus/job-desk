<?php

namespace Database\Seeders;

use App\Models\item;
use Illuminate\Database\Seeder;

class itemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        item::truncate();
  
        $csvFile = fopen(base_path("database/data/items.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== false) {
            if (!$firstline) {
                item::create([
                   'name' => $data[0],
                   'price' => $data[1],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
