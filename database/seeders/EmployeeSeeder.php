<?php

namespace Database\Seeders;

use App\Models\employee;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        employee::truncate();
  
        $csvFile = fopen(base_path("database/data/employee.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== false) {
            if (!$firstline) {
                employee::create([
                    'name' => $data[0],
                    'password' => bcrypt($data[1]),
                    'company_id' => $data[2],
                    'city' => $data[3],
                    'phone' => $data[4],
                    'email' => $data[5],
                    'created_users_id' => $data[6],
                    'updated_users_id' => $data[7]
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }
}
