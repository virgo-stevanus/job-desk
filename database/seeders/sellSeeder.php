<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\sell;
use App\Models\sellsummaries;
use Illuminate\Database\Seeder;

class sellSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        sell::truncate();
  
        $csvFile = fopen(base_path("database/data/sell.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== false) {
            if (!$firstline) {
                sell::create([
                   'created_date' => $data[0],
                   'item_id' => $data[1],
                   'price' => $data[2],
                   'discount' => $data[3],
                   'employee_id' => $data[4],
                ]);    

                $summaries = sellsummaries::where('date','=', Carbon::parse($data[0])->format('Y-m-d'))
                ->where('employee_id','=', $data[4])->first();
                $prtotal = empty($summaries) ? 0 : $data[2];
                $pricetotal = $prtotal + $data[2];
                $dsctotal = empty($summaries) ? 0 : $data[3];
                $discounttotal = $dsctotal + ( $data[3] * $data[2] / 100);
                $total = $pricetotal - $discounttotal;
        
                sellsummaries::updateOrCreate(
                    [
                        'date' => Carbon::parse($data[0])->format('Y-m-d'),
                        'employee_id' => $data[4]
                    ],
        
                    [   
                        'price_total' => $pricetotal,
                        'discount_total' => $discounttotal,
                        'total' => $total,
                    ]
                );
            }
            $firstline = false;

            
        }
        
       

        fclose($csvFile);
    }
}
