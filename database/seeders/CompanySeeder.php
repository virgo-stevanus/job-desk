<?php

namespace Database\Seeders;


use App\Models\companies;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        companies::truncate();
  
        $csvFile = fopen(base_path("database/data/company.csv"), "r");
  
        $firstline = true;
        while (($data = fgetcsv($csvFile, 2000, ",")) !== false) {
            if (!$firstline) {
                companies::create([
                    'company_name' => $data[0],
                    'address' => $data[1],
                    'city' => $data[2],
                    'country' => $data[3],
                    'created_users_id' => $data[4],
                    'updated_users_id' => $data[5]
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csvFile);
    }


}
